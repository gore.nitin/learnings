package com.designPattern.creational.builder;

import lombok.ToString;

@ToString
public class Computer {
	
	private String ram;
	private String hdd;
	private String cpu;
	private boolean isGraphicsCardEnabled;
	
	public Computer(ComputerBuilder builder) {
		this.ram = builder.ram;
		this.hdd = builder.hdd;
		this.cpu = builder.cpu;
		this.isGraphicsCardEnabled = builder.isGraphicsCardEnabled;
	}

	public String getRam() {
		return ram;
	}

	public String getHdd() {
		return hdd;
	}

	public String getCpu() {
		return cpu;
	}

	public boolean isGraphicsCardEnabled() {
		return isGraphicsCardEnabled;
	}
	 	 
	public static class ComputerBuilder {
		private String ram;
		private String hdd;
		private String cpu;
		private boolean isGraphicsCardEnabled;

		public ComputerBuilder(String ram, String hdd) {
			this.ram = ram;
			this.hdd = hdd;
		}

		public ComputerBuilder setCpu(String cpu) {
			this.cpu = cpu;
			return this;
		}

		public Computer build() {
			return new Computer(this);
		}

		public ComputerBuilder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
			this.isGraphicsCardEnabled = isGraphicsCardEnabled;
			return this;
		}
	}
	

}
