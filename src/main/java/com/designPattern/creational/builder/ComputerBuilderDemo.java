package com.designPattern.creational.builder;

public class ComputerBuilderDemo {	
	
	public static void main(String[] args) {
		Computer computer = new Computer
				.ComputerBuilder("2 GB", "500 GB")
				.setCpu("2.4 GHz")
				.build();
        System.out.println(computer);
    }
		
	}

