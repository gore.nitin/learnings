package com.designPattern.creational.factory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DesktopComputerFactory {

	private String ram;
	private String hdd;
	private String cpu;
   	
	public Computer createDesktopComputer() {
         return new DesktopComputer(ram, hdd, cpu);
	}

	
}
