package com.designPattern.creational.factory;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FactoryPatternDemo {

    public static void main(String[] args) {

	String computerType = "Server";
	String ram = "16 GB";
	String hdd = "1 TB";
	String cpu = "2.9 GHz";
	
	Computer computer = getComputer(computerType, ram, hdd, cpu);
	System.out.println("Computer Details: " + computer);

	
	String computerType2 = "Desktop";
	String ram2 = "8 GB";
	String hdd2 = "500 GB";
		
	Computer computer2 = getComputer(computerType2, ram2, hdd2, cpu);
	System.out.println("Computer Details: " + computer2);
	

    }

    public static Computer getComputer(String computerType, String ram, String hdd, String cpu) {

	if ("Server".equalsIgnoreCase(computerType)) {
	    return new ServerComputer(ram, hdd, cpu);
	} else if ("Desktop".equalsIgnoreCase(computerType)) {
	    return new DesktopComputer(ram, hdd, cpu);
	}

	return null;

    }

}
