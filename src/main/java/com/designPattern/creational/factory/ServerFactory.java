package com.designPattern.creational.factory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ServerFactory {

	private String ram;
	private String hdd;
	private String cpu;
  	
 	public Computer createComputer() {
		return new ServerComputer(ram, hdd, cpu);
	}

}
