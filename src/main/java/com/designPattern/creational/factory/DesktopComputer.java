package com.designPattern.creational.factory;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class DesktopComputer implements Computer {

 	private String ram;
	private String hdd;
	private String cpu;
	
	@Override
	public String getRAM() {
 		return this.ram;
	}

	@Override
	public String getHDD() {
		return this.hdd;
	}

	@Override
	public String getCPU() {
		return this.cpu;
    }
	
	@Override
	public String getType() {
		return "Desktop PC";
	}
	
}
