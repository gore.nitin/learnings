package com.designPattern.creational.factory;

public interface Computer {

    String getType();

    String getRAM();

    String getHDD();

    String getCPU();

}
