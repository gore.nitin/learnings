package com.designPattern.creational.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SingletonTester {
	
	private static EnumSingleton enumSingletonInstance = EnumSingleton.instance;
	
	public static void main(String[] args) {
		
		enumSingletonInstance.print("Hello Singleton Demo class");
		
		EagerSingleton eagerSingleton = EagerSingleton.getInstance();
		System.out.println(eagerSingleton);
		eagerSingleton.print("Hello Eager Singleton");
		
		EagerSingleton eagerSingleton2 = EagerSingleton.getInstance();
		System.out.println(eagerSingleton2);
		eagerSingleton.print("Hello Eager Singleton");
		
		LasySingleton lasySingleton = LasySingleton.getInstance();
		System.out.println(lasySingleton);
		lasySingleton.print("Hello Lasy Singleton");
		
		LasySingleton lasySingleton2 = LasySingleton.getInstance();
		System.out.println(lasySingleton2);
		lasySingleton.print("Hello Lasy Singleton");
		
		
		LasySingletonClonable lasySingletonClonable = LasySingletonClonable.getInstance();
		System.out.println(lasySingletonClonable);
		lasySingletonClonable.print("Hello Lasy Singleton Clonable");
		
		try {
 		    LasySingletonClonable lasySingletonClonableClone = lasySingletonClonable.clone();
 		    //THIS WILL NEVER CALLED
 		    System.out.println(lasySingletonClonable);
		
		} catch (CloneNotSupportedException e) {
		     e.printStackTrace();
		}
		
		SingletonSerializable singletonSerializable = SingletonSerializable.getInstance();
		System.out.println(singletonSerializable);
		singletonSerializable.print("Hello Singleton Serializable");
		
		
		 // Serialize object state to file
		ObjectOutputStream out;
		try {
		    out = new ObjectOutputStream(new FileOutputStream("codePumpkin.ser"));
		    
		        out.writeObject(singletonSerializable);
		        out.close();
		 
		        // deserialize from file to object
		        ObjectInput in = new ObjectInputStream(new FileInputStream("codePumpkin.ser"));
		        SingletonSerializable singletonSerializable2 = (SingletonSerializable) in.readObject();
		        in.close();
		 
		        System.out.println("instance1 hashCode = " + singletonSerializable.hashCode());
		        System.out.println("instance2 hashCode = " + singletonSerializable2.hashCode());
		    
		} catch (FileNotFoundException e) {
 		    e.printStackTrace();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (ClassNotFoundException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	 
	}

}
