package com.designPattern.creational.singleton;

import java.io.Serializable;

public class SingletonSerializable implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static SingletonSerializable instance;

    private SingletonSerializable() {
    }

    protected Object readResolve() {
	return getInstance();
    }

    public static SingletonSerializable getInstance() {
	if (instance == null) {
	    synchronized (SingletonSerializable.class) {
		if (instance == null) {
		    instance = new SingletonSerializable();
		}
	    }
	}
	return instance;
    }

    public void print(String string) {
	System.out.println(string);
    }

}
