package com.designPattern.creational.singleton;

public final class LasySingletonClonable implements Cloneable {

    private static LasySingletonClonable instance;

    private LasySingletonClonable() {
    }

    @Override
    protected LasySingletonClonable clone() throws CloneNotSupportedException {
	throw new CloneNotSupportedException("Clone Not Supported for this Singleton Class..");
    }
    
    public static LasySingletonClonable getInstance() {
	if (instance == null) {
	    synchronized (LasySingletonClonable.class) {
		if (instance == null) {
		    instance = new LasySingletonClonable();
		}
	    }
	}
	return instance;
     }

    public void print(String string) {
	System.out.println(string);
    }

}
