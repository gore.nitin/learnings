package com.designPattern.creational.singleton;

public enum EnumSingleton {

	instance;
	
	public void print(String message)
	{
		System.out.println(message);
	}
}
