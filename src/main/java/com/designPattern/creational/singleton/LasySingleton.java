package com.designPattern.creational.singleton;

public final class LasySingleton {

    private static LasySingleton instance;

    private LasySingleton() {
    }

    public static LasySingleton getInstance() {
	if (instance == null) {
	    synchronized (LasySingleton.class) {
		if (instance == null) {
		    instance = new LasySingleton();
		}
	    }
	}
	return instance;
     }

    public void print(String string) {
	System.out.println(string);
    }

}
