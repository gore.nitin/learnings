package com.designPattern.creational.singleton;

public final class EagerSingleton {

    private static final EagerSingleton instance = new EagerSingleton();
    
    private EagerSingleton() {
    }
    
    public static EagerSingleton getInstance() {
	return instance;
    }

    public void print(String string) {
	 System.out.println(string);
    }
     
}
