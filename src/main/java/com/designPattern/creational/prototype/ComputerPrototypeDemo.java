package com.designPattern.creational.prototype;

public class ComputerPrototypeDemo {

	public static void main(String[] args) throws CloneNotSupportedException {
  		
		ComputerPrototype computer = new ComputerPrototype();
		System.out.println(computer.getParts());
		
		ComputerPrototype clone = (ComputerPrototype) computer.clone();
		computer.getParts().remove("Mouse");
		computer.getParts().add("Optical Mouse");
		
		System.out.println(computer.getParts());
		System.out.println(clone.getParts());
	}
}
