package com.designPattern.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class ComputerPrototype implements Cloneable {

	private List<String> parts = new ArrayList<>();
	
	public ComputerPrototype() {
		parts.add("Monitor");
		parts.add("Keyboard");
		parts.add("Mouse");
	}
	
	public ComputerPrototype(List<String> parts) {
		this.parts = parts;
	}
	
	public List<String> getParts() {
		return parts;
	}
 	
	@Override
	public Object clone() throws CloneNotSupportedException {
		List<String> temp = new ArrayList<>();
		for (String s : this.getParts()) {
			temp.add(s);
		}
		return new ComputerPrototype(temp);
	}
}
