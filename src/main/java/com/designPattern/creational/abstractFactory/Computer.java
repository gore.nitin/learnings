package com.designPattern.creational.abstractFactory;

public interface Computer {

    String getType();

    String getRAM();

    String getHDD();

    String getCPU();

}
