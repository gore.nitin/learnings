package com.designPattern.creational.abstractFactory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DesktopFactory implements ComputerAbstractFactory {

	private String ram;
	private String hdd;
	private String cpu;
   	
 	public Computer createComputer() {
         return new Desktop(ram, hdd, cpu);
	}

	
}
