package com.designPattern.creational.abstractFactory;

public class ComputerAbstractFactoryDemo {

    public static void main(String[] args) {
	
	ComputerAbstractFactory desktopFactory = new DesktopFactory("2 GB", "500 GB", "2.4 GHz");
	
	Computer desktop = desktopFactory.createComputer();
	System.out.println("Desktop Computer::"+desktop);
	
    	
	ComputerAbstractFactory serverFactory = new ServerFactory("16 GB", "1 TB", "2.9 GHz");
	Computer server = serverFactory.createComputer();
	System.out.println("Server Computer ::"+server);
	
	
	
	
    }
}
