package com.designPattern.creational.abstractFactory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ServerFactory implements ComputerAbstractFactory {

	private String ram;
	private String hdd;
	private String cpu;
  	
 	public Computer createComputer() {
		return new Server(ram, hdd, cpu);
	}

}
