package com.designPattern.structural.adaptor;

public class Adaptor12Volt {
 
	private final Socket socket;
	
	public Adaptor12Volt(Socket socket) {
		this.socket = socket;
	}
	
	public Volt getVolt() {
		Volt v = socket.getVolt();
		return convertVolt(v, 10);
	}

	private Volt convertVolt(Volt v, int i) {
		return new Volt(v.getVolts() / i);
	}
 
}
