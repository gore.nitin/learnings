package com.designPattern.structural.adaptor;

public class AdaptorTester {

	public static void main(String[] args) {
		
		Socket socket = new Socket();
		System.out.println("Input from Main Socket : Volt "+ socket.getVolt());
		
		Adaptor12Volt adaptor12 = new Adaptor12Volt(socket);
		System.out.println("Output from 12 Volt Adaptor : Volt "+ adaptor12.getVolt());
	
		Adaptor5Volt adaptor5 = new Adaptor5Volt(socket);
		System.out.println("Output from 5 Volt Adaptor : Volt "+ adaptor5.getVolt());
	
	}
	
}
