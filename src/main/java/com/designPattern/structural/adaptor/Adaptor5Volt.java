package com.designPattern.structural.adaptor;

public class Adaptor5Volt {
  
	private Socket socket;

	Adaptor5Volt(Socket socket) {
		this.socket = socket;
	}
	
	public Volt getVolt() {
		Volt v = socket.getVolt();
		return convertVolt(v, 24);
	}

	private Volt convertVolt(Volt v, int i) {
		return new Volt(v.getVolts() / i);
	}

}
