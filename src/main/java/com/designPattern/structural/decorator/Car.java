package com.designPattern.structural.decorator;

public interface Car {

	public void assemble();

}
