package com.designPattern.structural.decorator;

public class ClientTest {

	public static void main(String[] args) {
		
		Car basicCar = new BasicCar();
		basicCar.assemble();
		
		System.out.println("\n");
		
		Car luxaryCar = new LuxaryCarDecorator(basicCar);
		luxaryCar.assemble();
		
		System.out.println("\n");
		
		Car superCar = new SuperCarDecorator(basicCar);
		superCar.assemble();

		System.out.println("\n");
		
		Car superLuxaryCar = new SuperCarDecorator(new LuxaryCarDecorator(new BasicCar()));
		superLuxaryCar.assemble();

		System.out.println("\n");

		Car luxarySuperCar = new LuxaryCarDecorator(new SuperCarDecorator(new BasicCar()));
		luxarySuperCar.assemble();

	}
	
}
