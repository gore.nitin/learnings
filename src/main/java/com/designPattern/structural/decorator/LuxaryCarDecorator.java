package com.designPattern.structural.decorator;

public class LuxaryCarDecorator extends CarDecorator {

	public LuxaryCarDecorator(Car c) {
		super(c);
 	}

	@Override
	public void assemble() {
		super.assemble();
		System.out.print(" Adding features of Luxary Car.");
	}
}
