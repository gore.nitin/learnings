package com.designPattern.structural.decorator;

public class BasicCar implements Car {

	@Override
	public void assemble() {
         System.out.print("Building a Basic Car.");		
	}

}
