package com.designPattern.structural.decorator;

public class SuperCarDecorator extends CarDecorator {

	public SuperCarDecorator(Car c) {
		super(c);
 	}
	
	@Override
	public void assemble() {
		super.assemble();
		System.out.print(" Adding features of Super Car.");
	}

}
