package com.designPattern.structural.proxy;

public class Tester {

	public static void main(String[] args) {
		CommandExecutor commandExecutor = new CommandExecutorImpl();
		commandExecutor.executeCommand("ls");
		commandExecutor.executeCommand("rm");
		commandExecutor.executeCommand("mkdir");
		commandExecutor.executeCommand("rmdir");
		commandExecutor.executeCommand("cp");
		commandExecutor.executeCommand("mv");
		
		CommandExecutor commandExecutorProxy = new CommandExecutorProxy("admin", "admin");
		commandExecutorProxy.executeCommand("ls");
		commandExecutorProxy.executeCommand("rm");
		commandExecutorProxy.executeCommand("mkdir");
		commandExecutorProxy.executeCommand("rmdir");
		commandExecutorProxy.executeCommand("cp");
		commandExecutorProxy.executeCommand("mv");

		CommandExecutor commandExecutorProxyNonAdmin = new CommandExecutorProxy("user", "user");
		commandExecutorProxyNonAdmin.executeCommand("ls");
		commandExecutorProxyNonAdmin.executeCommand("rm");
		commandExecutorProxyNonAdmin.executeCommand("mkdir");
		commandExecutorProxyNonAdmin.executeCommand("rmdir");
		commandExecutorProxyNonAdmin.executeCommand("cp");
		commandExecutorProxyNonAdmin.executeCommand("mv");

	}
}
