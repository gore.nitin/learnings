package com.designPattern.structural.proxy;

public interface CommandExecutor {

	void executeCommand(String command);
	
}
