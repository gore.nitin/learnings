package com.designPattern.structural.proxy;

public class CommandExecutorImpl implements CommandExecutor {

	@Override
	public void executeCommand(String command) {
		System.out.println("Executing command: " + command);
	}

}
