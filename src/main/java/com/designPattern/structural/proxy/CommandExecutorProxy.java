package com.designPattern.structural.proxy;

public class CommandExecutorProxy implements CommandExecutor {

	private boolean isAdmin;
	private CommandExecutor commandExecutor;
	
	public CommandExecutorProxy(String username, String password) {
		if (username.equals("admin") && password.equals("admin")) {
			isAdmin = true;
		}
		commandExecutor = new CommandExecutorImpl();
	}
	
	@Override
	public void executeCommand(String command) {
		if (isAdmin) {
			commandExecutor.executeCommand(command);
		} else {
			if (command.equals("rm") || command.equals("rmdir") || command.equals("cp") || command.equals("mv")) {
				System.out.println("You are not authorized to execute "+command+" command");
			} else {
				commandExecutor.executeCommand(command);
			}
		}
	}

}
