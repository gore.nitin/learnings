package com.designPattern.structural.facade;

import java.sql.Connection;

public class MySqlHelper {
	
	public static Connection getMySqlDBConnection(){
		System.out.println("Getting MySql DB Connection");
		return null;
	}
	
	public void generateMySqlPDFReport(String tableName, Connection con){
		System.out.println("Generating MySql PDF Report");
	}
	
	public void generateMySqlHTMLReport(String tableName, Connection con){
		System.out.println("Generating MySql HTML Report");
	}
}