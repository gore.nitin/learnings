package com.designPattern.structural.facade;

import java.sql.Connection;

public class OracleHelper {

	public static Connection getOracleDBConnection(){
		System.out.println("Getting Oracle DB Connection");
		return null;
	}
	
	public void generateOraclePDFReport(String tableName, Connection con){
		System.out.println("Generating Oracle PDF Report");
	}
	
	public void generateOracleHTMLReport(String tableName, Connection con){
		System.out.println("Generating Oracle HTML Report");
	}
	
}