package com.designPattern.structural.bridge;

public class CircleShape extends Shape {

	public CircleShape(Color color) {
		super(color);
	}

	@Override
	public void applyColor() {
		System.out.println("Creating A Circle Shape Color.");
		color.applyColor();
	}
}
