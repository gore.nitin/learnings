package com.designPattern.structural.bridge;

public class BlueColor implements Color {

	@Override
	public void applyColor() {
		System.out.println("Appling Color Blue.");
	}
}
