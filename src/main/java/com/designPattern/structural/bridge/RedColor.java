package com.designPattern.structural.bridge;

public class RedColor implements Color {

	@Override
	public void applyColor() {
		System.out.println("Appling Color Red.");
	}
}
