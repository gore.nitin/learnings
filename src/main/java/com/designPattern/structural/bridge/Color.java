package com.designPattern.structural.bridge;

public interface Color {

	public void applyColor();
	
}
