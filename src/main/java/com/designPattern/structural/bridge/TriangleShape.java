package com.designPattern.structural.bridge;

public class TriangleShape extends Shape {

	public TriangleShape(Color color) {
		super(color);
	}

	@Override
	public void applyColor() {
		System.out.println("Creating a Triangle Shape with No Color.");
		color.applyColor();
	}
	
}
