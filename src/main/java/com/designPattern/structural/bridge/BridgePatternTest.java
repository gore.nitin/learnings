package com.designPattern.structural.bridge;

public class BridgePatternTest {

	public static void main(String[] args) {

		RedColor red = new RedColor();
		GreenColor green = new GreenColor();
		BlueColor blue = new BlueColor();
		
		Shape triangle = new TriangleShape(red);
		triangle.applyColor();

		Shape circle = new CircleShape(green);
		circle.applyColor();

		circle = new CircleShape(blue);
 		circle.applyColor();
		
	}

}
