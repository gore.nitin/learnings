package com.designPattern.structural.bridge;

public class GreenColor implements Color {

	@Override
	public void applyColor() {
		System.out.println("Appling Color Green.");
	}
}
