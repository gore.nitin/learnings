package com.designPattern.structural.composit;

public interface Elements {
	public float getPrice();
}
