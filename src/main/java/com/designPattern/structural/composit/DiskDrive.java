package com.designPattern.structural.composit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString
public class DiskDrive implements Elements {

	private String brandName;
	private String type;
	private float speed;
	private float price;

}
