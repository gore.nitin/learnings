package com.designPattern.structural.composit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Ram implements Elements {

	private String brandName;
	private int frequency;
	private int capacity;
	private float price;
	
}
