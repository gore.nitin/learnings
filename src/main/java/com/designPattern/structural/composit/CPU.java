package com.designPattern.structural.composit;

public class CPU extends Component {

	private static final int DISCOUNT = 10;

	public CPU(String name, Processor processor, DiskDrive diskDrive, Ram ram) {
		this.name = name;
		components.add(processor);
		components.add(diskDrive);
		components.add(ram);
	}

	@Override
	public float getDiscoutedPrice() {
		return getPrice() - (getPrice() * DISCOUNT / 100);
	}

	@Override
	public String toString() {
        return "CPU [name=" + name + ", components=" + components + "]";
    }
}
