package com.designPattern.structural.composit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Monitor implements Elements {
	
	private String name;
	private String screenSize;
	private float price;
	
}
