package com.designPattern.structural.composit;

public class ComputerTester {

	
	public static void main(String[] args) {
	
		Ram ram2Gb = new Ram("Kingstone", 1600 , 2024, 800);
		Ram ram4Gb = new Ram("Kingstone", 1600 , 2024, 1200);
		
		Mouse logiTechMouse = new Mouse("Logitech", 500);
		Mouse iBallMouse = new Mouse("Iball", 300);
		
		KeyBoard logiTechBoard = new KeyBoard("Logitech", 600);
		KeyBoard iballKeyBoard = new KeyBoard("iBall", 400);

		Processor i3 = new Processor("i3", 3.2f, 5000);
 		Processor i7 = new Processor("i7", 2.2f, 12000);
		
		DiskDrive kingstone = new DiskDrive("kingstone","DDR2", 500, 3000);
		DiskDrive seagate = new DiskDrive("segate","DDR2", 500, 3000);
 		
		CPU customCpu = new CPU("Custom CPU",i3, kingstone, ram2Gb);
		CPU lenovoCpu = new CPU("Lenovo CPU",i7, seagate, ram4Gb);

		Monitor asusMonitor = new Monitor("Asus", "18 inch", 12000);
		Monitor dellMonitor = new Monitor("Dell", "14 inch", 8000);

		System.out.println("1. ************************************");

		customCpu.showElements();
		System.out.println("custom Cpu Total Price: " + customCpu.getPrice() + " Discounted Price: " + customCpu.getDiscoutedPrice());

		System.out.println("2. ************************************");
	 	lenovoCpu.showElements();
		System.out.println("lenovo Cpu Total Price: " + lenovoCpu.getPrice() + " Discounted Price: " + lenovoCpu.getDiscoutedPrice());

		System.out.println("3. ************************************");
	 	DesktopPc customDesktopPc = new DesktopPc("Custom Desktop PC", customCpu, logiTechMouse, logiTechBoard);
	 	customDesktopPc.addElement(dellMonitor);
		customDesktopPc.showElements();
		System.out.println("custom Desktop Total Price: " + customDesktopPc.getPrice() + " Discounted Price: " + customDesktopPc.getDiscoutedPrice());

		System.out.println("4. ************************************");
	 	DesktopPc lenovoDesktopPc = new DesktopPc("Lenovo Desktop PC", lenovoCpu, iBallMouse, iballKeyBoard);
		lenovoDesktopPc.addElement(asusMonitor);
		lenovoDesktopPc.showElements();
		System.out.println("custom Desktop Total Price: " + lenovoDesktopPc.getPrice() + " Discounted Price: " + lenovoDesktopPc.getDiscoutedPrice());

	}
	
}
