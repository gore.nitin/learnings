package com.designPattern.structural.composit;

import lombok.ToString;

@ToString
public class DesktopPc extends Component {
	
	private static final int DISCOUNT = 20;
	
	public DesktopPc(String name, CPU cpu, Mouse mouse, KeyBoard keyBoard) {
		this.name = name;
		components.add(cpu);
		components.add(mouse);
		components.add(keyBoard);
	}
	
	public void addElement(Elements element) {
	 	this.addComponent(element);
	}
	
	@Override
	public float getDiscoutedPrice() {
		return getPrice() - (getPrice() * DISCOUNT / 100);
 	}
    
}
