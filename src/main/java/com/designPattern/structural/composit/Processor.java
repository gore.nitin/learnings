package com.designPattern.structural.composit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Processor implements Elements {
	
	private String name;
	private float speed;
	private float price;
}
