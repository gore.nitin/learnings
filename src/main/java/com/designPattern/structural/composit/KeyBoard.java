package com.designPattern.structural.composit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class KeyBoard implements Elements {
	
	private String name;
	private float price;
	
}
