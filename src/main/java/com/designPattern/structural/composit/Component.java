package com.designPattern.structural.composit;

import java.util.ArrayList;
import java.util.List;

public abstract class Component implements Elements {
	
	protected String name;

	protected List<Elements> components = new ArrayList<>();
	
	public abstract float getDiscoutedPrice();
 	
	public void showElements() {
		System.out.println("Component: " + name);
 		for (Elements element : components) {
			if(element instanceof Component) {
                ((Component) element).showElements();
			}
 			System.out.println(element);
		}
	}

	public void addComponent(Elements component) {
		components.add(component);
	}

	public void removeComponent(Elements component) {
		components.remove(component);
	}

	@Override
	public float getPrice() {
		float totalPrice = 0;
		for (Elements element : components) {
			totalPrice += element.getPrice();
		}
		return totalPrice;
	}
 	
}
