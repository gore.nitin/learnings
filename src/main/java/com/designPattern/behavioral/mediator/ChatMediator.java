package com.designPattern.behavioral.mediator;
public interface ChatMediator {

	public void sendMessage(String msg, User user);
	
 	public void sendMessage(String msg, User fromUser, User toUser);

	void addUser(User user);
}