package com.designPattern.behavioral.mediator;
public class UserImpl extends User {

	public UserImpl(ChatMediator med, String name) {
		super(med, name);
	}

	@Override
	public void send(String msg){
		System.out.println(this.name+": Sending Message="+msg);
		mediator.sendMessage(msg, this);
	}
	
	@Override
	public void send(User user,String msg){
		System.out.println(this.name+": Sending Message="+msg);
		mediator.sendMessage(msg, this, user);
	}
	
	@Override
	public void receive(String msg) {
		System.out.println(this.name+": Received Message:"+msg);
	}

}