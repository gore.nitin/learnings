package com.designPattern.behavioral.iterator;
public enum ChannelTypeEnum {

	ENGLISH, HINDI, FRENCH, ALL;
}