package com.designPattern.behavioral.iterator;

public interface ChannelIterator {

	public boolean hasNext();
	
	public Channel next();
}