package com.designPattern.behavioral.command;

public interface Command {

	void execute();
}