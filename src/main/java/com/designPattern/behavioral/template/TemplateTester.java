package com.designPattern.behavioral.template;

public class TemplateTester {

	public static void main(String[] args) {
        HouseTemplate houseType = new WoodenHouse();
        houseType.buildHouse();
        
        System.out.println("************");
        
        houseType = new GlassHouse();
        houseType.buildHouse();
        
        System.out.println("************");
                
        houseType = new BambooHouse();
        houseType.buildHouse();
        
    }
}
