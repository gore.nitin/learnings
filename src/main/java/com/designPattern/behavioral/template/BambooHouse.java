package com.designPattern.behavioral.template;

public class BambooHouse extends HouseTemplate {

// Protect some methods to prevent overriding in the child classes
//	@Override 
//	public void buildHousePlan() {
//		System.out.println("Building Bamboo house plan..");
//	}
	
	@Override
	public void buildWindows() {
		System.out.println("Building windows with Bamboo sticks");
	}
	
	@Override
	public void buildWalls() {
		System.out.println("Building Bamboo Walls");
	}

	@Override
	public void buildPillars() {
		System.out.println("Building Pillars with Bamboo coating");
	}
	
	@Override
	protected void buildFoundation() {
		System.out.println("Building foundation with Bamboo sticks and mud");
	}
}
