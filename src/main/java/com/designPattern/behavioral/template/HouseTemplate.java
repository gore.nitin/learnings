package com.designPattern.behavioral.template;

public abstract class HouseTemplate {

	// template method, final so subclasses can't override
	public final void buildHouse() {
		buildHousePlan();
		buildFoundation();
		buildPillars();
		buildWalls();
		buildWindows();
		System.out.println("House is built.");
	}

	private final void buildHousePlan() {
		System.out.println("Building house plan..");
	}

	// default implementation but can override if needed
	protected void buildWindows() {
		System.out.println("Building Glass Windows");
	}

	// methods to be implemented by subclasses
	public abstract void buildWalls();

	public abstract void buildPillars();

	//May have required or default implementation
	protected void buildFoundation() {
		System.out.println("Building foundation with cement,iron rods and sand");
	}

}
