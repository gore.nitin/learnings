package com.designPattern.behavioral.chainOfResponsibility;

public class Dollar1Dispenser implements DispenseChain {

    private DispenseChain chain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
	this.chain = nextChain;
    }

    @Override
    public void dispense(Currency cur) {
	if (cur.getAmount() >= 1) {
	    int num = cur.getAmount();
	    int remainder = cur.getAmount();
	    System.out.println("Dispensing " + num + " 1$ note");
	} else {
	    this.chain.dispense(cur);
	}
    }

}