package com.designPattern.behavioral.state;
public interface State {

	public void doAction();
}